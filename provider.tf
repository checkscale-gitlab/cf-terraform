terraform {
  required_providers {
    cloudflare = {
      source = "cloudflare/cloudflare"
      version = "~> 3.0"
    }
  }
}

variable "cloudflare_zone_id" {
  type = string
}

provider "cloudflare" {

}

data "cloudflare_zones" "allzones" {
  filter {}
}

output "domain_zones" {
  value = data.cloudflare_zones.allzones
}